# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: MIT

---
- hosts: all
  become: true
  vars_files:
    - vars/default.yml

  tasks:
    # Install base packages and configure Apache
    - name: Install LAMP packages
      ansible.builtin.apt:
        name: "{{ item }}"
        update_cache: yes
        state: present
      loop:
        - apache2
        - mariadb-server
        - python3-pymysql
        - php
        - libapache2-mod-php

    - name: Install PHP extensions
      ansible.builtin.apt:
        name: "{{ item }}"
        update_cache: yes
        state: present
      loop: "{{ php_extensions }}"
      notify: Restart Apache

    - name: Ensure document root exists
      ansible.builtin.file:
        path: "/var/www/{{ apache_host }}"
        state: directory
        owner: "www-data"
        group: "www-data"
        mode: 0755

    - name: Configure Apache VirtualHost
      ansible.builtin.template:
        src: "templates/apache.conf.j2"
        dest: "/etc/apache2/sites-available/{{ apache_configuration_file }}"
      notify: Reload Apache

    - name: Enable rewrite module
      community.general.apache2_module: 
        name: rewrite
        state: present
      notify: Reload Apache

    - name: Enable site
      ansible.builtin.command: "/usr/sbin/a2ensite {{ apache_configuration_file }}"
      register: a2ensite_output
      changed_when: '"Enabling site" in a2ensite_output.stdout'
      notify: Reload Apache

    # Configure MySQL
    - name: Create database for Wordpress
      community.mysql.mysql_db:
        name: "{{ mariadb_db }}"
        state: present
        login_unix_socket: /var/run/mysqld/mysqld.sock

    - name: Create MySQL user for Wordpress
      community.mysql.mysql_user:
        name: "{{ mariadb_user }}"
        password: "{{ mariadb_password }}"
        priv: "{{ mariadb_db }}.*:ALL"
        state: present
        login_unix_socket: /var/run/mysqld/mysqld.sock

    # Install and configure Wordpress
    - name: Download and unpack Wordpress
      ansible.builtin.unarchive:
        src: https://wordpress.org/wordpress-6.2.2.tar.gz
        dest: "/var/www/{{ apache_host }}"
        remote_src: yes
        creates: "/var/www/{{ apache_host }}/wordpress"

    - name: Set directory permissions for Wordpress
      ansible.builtin.file:
        dest: "/var/www/{{ apache_host }}/wordpress"
        recurse: yes
        owner: www-data
        group: www-data
        mode: "u=rwX,g=rX,o="

    - name: Place wordpress configuration on the host
      ansible.builtin.template:
        src: "templates/wp-config.php.j2"
        dest: "/var/www/{{ apache_host }}/wordpress/wp-config.php"
        owner: www-data
        group: www-data
        mode: 0440

  handlers:
    - name: Reload Apache
      ansible.builtin.service:
        name: apache2
        state: reloaded

    - name: Restart Apache
      ansible.builtin.service:
        name: apache2
        state: restarted
