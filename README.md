# Ansible LAMP Wordpress Demo

This project contains an Ansible demo installing a LAMP Wordpress stack
on a Debian machine.
This example is **not** meant for production deployment.
It only serves to showcase Ansible.

## Install Requirements

* [Install Vagrant](https://developer.hashicorp.com/vagrant/downloads)
* Both the `libvirt` and `virtualbox` Vagrant provider are supported
* Make sure Ansible is installed on your system; this can be achieved via e.g. pipenv in this project

    ```bash
    pipenv install
    pipenv shell
    ```

## Start and provision the Vagrant machine

Starting the demo setup is as easy as running the command
`vagrant up` in the root of this project.

Reprovisiong the Ansible setup in Vagrant can be done by running the command
`vagrant provision`.

To stop the deployment run `vagrant halt`.
The machine can be entirely removed via `vagrant destroy`

## Access the Deployment

The deployment can be accessed via the URL:

<https://kroki-192-168-10-24.nip.io/>
