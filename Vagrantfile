# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: MIT

Vagrant.require_version ">= 1.8.0"

Vagrant.configure(2) do |config|

  config.vm.box = "debian/bookworm64"
  config.vm.network "private_network", ip: "192.168.10.24"
  config.nfs.verify_installed = false
  config.vm.synced_folder '.', '/vagrant', disabled: true

  config.vm.provider "virtualbox" do |vb|
    vb.name = "lamp_demo"
    vb.memory = 4096
    vb.cpus = 2
  end

  config.vm.provider :libvirt do |v|
    v.qemu_use_session = false
    v.memory = 4096
    v.cpus = 2
  end

  config.vm.provision "ansible" do |ansible|
    ansible.compatibility_mode = "2.0"
    ansible.limit = "vagrant"
    ansible.inventory_path = "hosts.yml"
    ansible.playbook = "playbook.yml"
  end
end
